﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleDialogBox : MonoBehaviour
{
    public Transform trackTarget;

    public Transform arrow;

    public float dialogHeight;

    public float followSpeed;

    public float 

    private void Start()
    {
        Debug.Log(Vector2.Distance(Camera.main.WorldToScreenPoint(trackTarget.position), arrow.position));
    }
    private void Update()
    {
        Vector2 _pos = Camera.main.WorldToScreenPoint(trackTarget.position);
        Vector2 arrowPos = arrow.position;
        transform.position = new Vector3(Mathf.Lerp(transform.position.x, _pos.x, followSpeed), _pos.y + dialogHeight);
        float angle = Vector2.Angle(Vector2.down, _pos - arrowPos);
        if (_pos.x < arrow.position.x)
            angle = -angle;
        arrow.localEulerAngles = new Vector3(0, 0, angle);
        float distance = Vector2.Distance(_pos, arrowPos);
        arrow.localScale = new Vector3(1.0f, distance / 270.0f, 1.0f);
    }

    private IEnumerator ShowDialogBox()
    {

        return null;
    }
}
