﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private static PlayerController _instance;
    public static PlayerController Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.Find("PlayerController").GetComponent<PlayerController>();
            return _instance;
        }
    }

    public Transform player;

    public float jumpStrength;

    //运动速度系数
    public float velocity;

    private void Update()
    {
        player.position += new Vector3(Input.GetAxis("Horizontal"), 0, 0) * velocity;
        if (Input.GetKeyDown(KeyCode.Space))
            player.GetComponent<Rigidbody>().AddForce(Vector3.up * jumpStrength);

    }
}
