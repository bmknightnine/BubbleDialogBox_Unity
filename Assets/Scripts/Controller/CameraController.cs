﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private static CameraController _instance;
    public static CameraController Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.Find("CameraController").GetComponent<CameraController>();
            return _instance;
        }
    }

    public Transform _camera;

    //跟踪速度
    public float followSpeed;

    private void Update()
    {
        _camera.position = new Vector3(
            Mathf.Lerp(_camera.position.x, PlayerController.Instance.player.position.x, followSpeed),
            _camera.position.y,
            _camera.position.z);
    }

}
